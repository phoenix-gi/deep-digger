package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Animation;
import net.phoenixgi.pgige.shape.CrossTester;
import net.phoenixgi.pgige.shape.Rectangle;
import net.phoenixgi.pgige.shape.Shape;

public class Digger extends Movable {

    private int health = 100;
    private int healthMax = 100;

    public final static int DIG_NO_DIRECTION = 0;
    public final static int DIG_LEFT = 1;
    public final static int DIG_RIGHT = 2;
    public final static int DIG_UP = 3;
    public final static int DIG_DOWN = 4;

    private boolean isTryingDig = false;
    private boolean isDigging = false;
    private int digDirection = DIG_NO_DIRECTION;
    private float waitTimeAfterDig = 0.5f;
    private float waitUntilNextDig = waitTimeAfterDig;
    private float digPower = 50f;
    private boolean attackMade = false;
    private boolean isAttack = false;
    private double attackDelay = 0.5f;
    private double attackTimePassed = 0;
    private double attackPower = 10;

    Animation moveAnimation;
    Animation digAnimation;
    Animation digDownAnimation;
    Animation attackAnimation;

    public Digger(int width, int height) {
        super(width, height);
        moveAnimation = new Animation(8, 16f);
        digAnimation = new Animation(25, 50f);
        digDownAnimation = new Animation(24, 48f);
        attackAnimation = new Animation(25, 50f);
        attackAnimation.setLoop(false);
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public boolean isTryingDig() {
        return isTryingDig;
    }

    public void setTryingDig(boolean tryingDig) {
        isTryingDig = tryingDig;
    }

    public void setDigDirection(int digDirection) {
        this.digDirection = digDirection;
    }

    public int getDigDirection() {
        return digDirection;
    }


    public int getHealthMax() {
        return healthMax;
    }

    public void updateDigging(float deltaTime) {
        waitUntilNextDig -= deltaTime;
        if (waitUntilNextDig < 0) {
            waitUntilNextDig = 0;
        }
    }

    public void updateAnimation(float deltaTime) {
        if (isInAttack()) {
            attackAnimation.update(deltaTime);
        } else {
            attackAnimation.frame = 0;
        }
        if (!isDigging() && !isInAttack()) {
            updateMoveAnimation(deltaTime);
            digDownAnimation.frame = 0;
            digAnimation.frame = 0;
        } else {
            moveAnimation.frame = 0;
            switch (digDirection) {
                case DIG_DOWN:
                    digDownAnimation.update(deltaTime);
                    break;
                case DIG_LEFT:
                case DIG_RIGHT:
                    digAnimation.update(deltaTime);
                    break;
            }
        }
    }

    public void updateMoveAnimation(float deltaTime) {
        if (getMovementState() == Movable.STATE_MOVEMENT_LEFT || getMovementState() == Movable.STATE_MOVEMENT_RIGHT) {
            moveAnimation.update(deltaTime);
        } else {
            moveAnimation.frame = 0;
        }
    }

    public void resetDigging() {
        waitUntilNextDig = waitTimeAfterDig;
    }

    public boolean readyForDig() {
        return waitUntilNextDig == 0;
    }

    public float getDigPower() {
        return digPower;
    }

    public Animation getMoveAnimation() {
        return moveAnimation;
    }

    public Animation getDigAnimation() {
        return digAnimation;
    }

    public Animation getDigDownAnimation() {
        return digDownAnimation;
    }

    public void setDigging(boolean digging) {
        isDigging = digging;
    }

    public boolean isDigging() {
        return isDigging;
    }

    public void setDigPower(float digPower) {
        this.digPower = digPower;
    }

    public void setAttack(boolean attack) {
        isAttack = attack;
    }

    public void attack() {
        if (!isAttackMade()) {
            isAttack = true;
            attackTimePassed = 0;
            attackAnimation.frame = 0;
        }
    }

    private void stopAttack() {
        isAttack = false;
        attackMade = false;
    }

    public boolean isInAttack() {
        return isAttack;
    }

    public boolean isAttackMade() {
        return attackMade;
    }

    public void makeAttack() {
        attackMade = true;
    }

    public double getAttackDelay() {
        return attackDelay;
    }

    public double getAttackTimePassed() {
        return attackTimePassed;
    }


    public void setAttackPower(double attackPower) {
        this.attackPower = attackPower;
    }


    public void updateAttack(double deltaTime) {
        if (isAttack) {
            attackTimePassed += deltaTime;
            if (attackTimePassed >= attackDelay) {
                stopAttack();
            }
        }
    }


    public boolean isUnderAttack(Digger other) {
        Shape otherShape = other.getShape();
        Rectangle attackRect = new Rectangle(otherShape.getWidth() + 32, other.getShape().getHeight());
        attackRect.move(otherShape.getCenter().x + otherShape.getWidth() / 2, 0);
        attackRect.move(0, otherShape.getCenter().y + otherShape.getHeight() / 2);
        Shape thatShape = getShape();
        Rectangle thatRect = new Rectangle(thatShape.getWidth(), thatShape.getHeight());
        thatRect.move(thatShape.getCenter().x + thatShape.getWidth() / 2, thatShape.getCenter().y + thatShape.getHeight() / 2);
        return CrossTester.isCross(thatRect, attackRect);
    }

    public void takeDamage(Digger other) {
        health -= other.attackPower;
        if (health < 0) {
            health = 0;
        }
    }

    public Animation getAttackAnimation() {
        return attackAnimation;
    }

    public void setAttackDelay(double attackDelay) {
        this.attackDelay = attackDelay;
    }
}
