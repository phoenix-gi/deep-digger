package net.phoenixgi.deep_digger;

import java.awt.*;

public class DiggerView extends RectangleIntView {
    private boolean showHealth;

    public DiggerView(Movable gameObject) {
        super(gameObject, 0x0000FF);
    }

    public DiggerView(Movable gameObject, int color) {
        super(gameObject, color);
    }

    public void setShowHealth(boolean showHealth) {
        this.showHealth = showHealth;
    }

    @Override
    public void render(Graphics g) {
        Digger digger = (Digger) gameObject;
        if (Config.debugMode) {
            super.render(g);
            String state = "";
            switch (digger.getState()) {
                case Movable.STATE_IN_FALL:
                    state = "in fall";
                    break;
                case Movable.STATE_ON_GROUND:
                    state = "on ground";
                    break;
            }
            RectangleInt shape = digger.getShape();
            int h = (int) (shape.getHeight() * (float) digger.getHealth() / (float) digger.getHealthMax());
            g.fillRect((int) shape.getLeftX(), (int) shape.getTopY(), (int) shape.getWidth(), h);
            ((Graphics2D) g).drawString(state, gameObject.getShape().getCenter().x, gameObject.getShape().getCenter().y);
        } else {
            int transform = 0;
            int x = (int) digger.getShape().getCenter().x - 8;
            int y = (int) digger.getShape().getCenter().y;
            if (digger.getDirection() == Movable.DIRECTION_LEFT) {
                transform = 1;
            }

            Assets.diggerMovingSprite.setFrameIndex(digger.getMoveAnimation().frame);
            Assets.diggerDiggingDownSprite.setFrameIndex(digger.getDigDownAnimation().frame);
            Assets.diggerDiggingSprite.setFrameIndex(digger.getDigAnimation().frame);
            if (digger.isInAttack()) {
                Assets.diggerDiggingSprite.setFrameIndex(digger.getAttackAnimation().frame);
                Assets.diggerDiggingSprite.paint(g, x - 8, y, transform);
            } else {
                if (digger.isDigging()) {
                    switch (digger.getDigDirection()) {
                        case Digger.DIG_LEFT:
                        case Digger.DIG_RIGHT:
                            Assets.diggerDiggingSprite.paint(g, x - 8, y, transform);
                            break;
                        case Digger.DIG_DOWN:
                            Assets.diggerDiggingDownSprite.paint(g, x - 8, y, transform);
                            break;
                    }
                } else {
                    Assets.diggerMovingSprite.paint(g, x, y, transform);
                }
            }

            if (showHealth && digger.getHealth() > 0) {
                int width = (int) (digger.getShape().getWidth() * digger.getHealth() / (float) digger.getHealthMax());
                int shiftX = ((int) digger.getShape().getWidth() - width) / 2;
                g.setColor(new Color(0x6F0000));
                g.fillRect(x + 6 + shiftX, y - 10, width + 3, 8);
                g.setColor(Color.RED);
                g.fillRect(x + 8 + shiftX, y - 8, width, 4);
            }
        }
    }
}
