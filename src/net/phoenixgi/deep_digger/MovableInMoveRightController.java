package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.object.GameObject;

/**
 * @author phoenix-gi
 */
public class MovableInMoveRightController extends ObjectController {

    private MainScene scene;

    public MovableInMoveRightController(Movable gameObject, MainScene scene) {
        super(gameObject);
        this.scene = scene;
    }

    public void update(float deltaTime) {
        Movable movable = (Movable) gameObject;

        movable.getVelocity().x = movable.getMovementSpeed();

        GameObject rightWall = scene.getRightWallFor(movable);
        float rightWallX = Level.LEVEL_WIDTH - 1;
        if (rightWall != null) {
            rightWallX = rightWall.getShape().getLeftX();
        }

        movable.getShape().move(movable.getVelocity().x * deltaTime, 0);
        if (movable.getShape().getRightX() > rightWallX) {
            movable.getShape().getCenter().x = rightWallX - movable.getShape().getWidth() - 1;
        }
        movable.setDirection(Movable.DIRECTION_RIGHT);
    }
}
