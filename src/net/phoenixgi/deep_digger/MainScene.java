package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.*;
import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.pc.StandardInput;
import net.phoenixgi.pgige.SpatialGridHelper;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MainScene extends Scene {
    DiggerController diggerController;

    Level level;

    public MainScene(SceneManager sceneManager, Game game) {
        super(sceneManager, game);
        level = new Level(this);
        level.generateNext();

        diggerController = new DiggerController(level.getDigger(), this);
    }

    @Override
    public void initialize() {
        DiggerInput diggerInput = new DiggerInput(diggerController, level);
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_F11:
                        getGame().setFullScreen(!getGame().isFullScreen());
                        break;
                    default:
                        diggerInput.keyPressed(keyEvent);
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                diggerInput.keyReleased(keyEvent);
            }
        });
    }

    @Override
    public void update(float deltaTime) {
        diggerController.update(deltaTime);
        level.update(deltaTime);
        if (level.getDigger().getHealth() <= 0) {
            getSceneManager().setScene(new GameOverScene(getSceneManager(), getGame(), level.getScore()));
        }
    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g2d = (Graphics2D) drawingContext;

        g2d.drawImage(Assets.background, 0, 0, null);
        int shiftX = 0;
        int shiftY = (int) -level.getDigger().getShape().getBottomY() + getHeight() / 2;
        g2d.translate(shiftX, shiftY);

        for (Object groundView : level.getGroundViews()) {
            ((GroundView) groundView).render(g2d);
        }
        for (Object rubyView : level.getRubyViews()) {
            ((RubyView) rubyView).render(g2d);
        }
        for (Object enemyView : level.getEnemyViews()) {
            ((DiggerView) enemyView).render(g2d);
        }
        level.getDiggerView().render(g2d);

        if (Config.debugMode) {
            if (level.getLeftWall() != null) {
                int x = (int) level.getLeftWall().getShape().getRightX();
                int y1 = (int) level.getLeftWall().getShape().getTopY();
                int y2 = (int) level.getLeftWall().getShape().getBottomY();
                g2d.setColor(Color.RED);
                g2d.drawLine(x, y1, x, y2);
            }

            if (level.getRightWall() != null) {
                int x = (int) level.getRightWall().getShape().getLeftX();
                int y1 = (int) level.getRightWall().getShape().getTopY();
                int y2 = (int) level.getRightWall().getShape().getBottomY();
                g2d.setColor(Color.RED);
                g2d.drawLine(x, y1, x, y2);
            }

            if (level.getTopWall() != null) {
                int x1 = (int) level.getTopWall().getShape().getLeftX();
                int x2 = (int) level.getTopWall().getShape().getRightX();
                int y = (int) level.getTopWall().getShape().getBottomY();
                g2d.setColor(Color.RED);
                g2d.drawLine(x1, y, x2, y);
            }

            if (level.getBottomWall() != null) {
                int x1 = (int) level.getBottomWall().getShape().getLeftX();
                int x2 = (int) level.getBottomWall().getShape().getRightX();
                int y = (int) level.getBottomWall().getShape().getTopY();
                g2d.setColor(Color.RED);
                g2d.drawLine(x1, y, x2, y);
            }
        }
        g2d.translate(-shiftX, -shiftY);
        Digger digger = level.getDigger();
        int width = (int) (256 * digger.getHealth() / (float) digger.getHealthMax());
        g2d.setColor(new Color(0x6F0000));
        g2d.fillRect(10 + 6, 10, width + 3, 20);
        g2d.setColor(Color.RED);
        g2d.fillRect(10 + 8, 12, width, 16);
        g2d.setFont(new Font("Arial", Font.BOLD, 32));
        g2d.setColor(new Color(0x6F0000));
        FontMetrics fm = g2d.getFontMetrics();
        g2d.fillRect(20, getHeight() - fm.getHeight() - 20, fm.stringWidth(String.valueOf(level.getScore())) + 4, fm.getHeight() + 4);
        g2d.setColor(Color.GREEN);
        g2d.drawString(String.valueOf(level.getScore()), 20 + 2, getHeight() - 28 + 2);
    }

    @Override
    public void destroy() {

    }


    public GameObject getBottomSurfaceFor(Movable movable) {
        GameObject bottomSurface = SpatialGridHelper.getBottomSurfaceFor(movable, level.getGroundGrid());
        GameObject bottomDynamicSurface = SpatialGridHelper.getBottomSurfaceFor(movable, level.getDynamicGrid());
        if (bottomSurface != null) {
            if (bottomDynamicSurface != null) {
                float bottomSurfaceY = bottomSurface.getShape().getTopY();
                float bottomDynamicSurfaceY = bottomDynamicSurface.getShape().getTopY();
                if (bottomSurfaceY < bottomDynamicSurfaceY) {
                    return bottomSurface;
                } else {
                    return bottomDynamicSurface;
                }
            } else {
                return bottomSurface;
            }
        } else {
            return bottomDynamicSurface;
        }
    }

//    public float getTopSurfaceYFor(Movable movable) {
//        float topSurfaceY = SpatialGridHelper.getTopSurfaceYFor(movable, level.getGroundGrid());
//        float topDynamicSurfaceY = SpatialGridHelper.getTopSurfaceYFor(movable, level.getDynamicGrid());
//        return Math.max(topSurfaceY, topDynamicSurfaceY);
//    }

    public GameObject getLeftWallFor(Movable movable) {
        GameObject leftWall = SpatialGridHelper.getLeftWall(movable, level.getGroundGrid());
        GameObject leftDynamicWall = SpatialGridHelper.getLeftWall(movable, level.getDynamicGrid());

        if (leftWall != null) {
            if (leftDynamicWall != null) {
                float leftWallX = leftWall.getShape().getRightX();
                float leftDynamicWallX = leftDynamicWall.getShape().getRightX();
                if (leftDynamicWallX > leftWallX) {
                    return leftDynamicWall;
                } else {
                    return leftWall;
                }
            } else {
                return leftWall;
            }
        } else {
            return leftDynamicWall;
        }
    }

    public GameObject getRightWallFor(Movable movable) {
        GameObject rightWall = SpatialGridHelper.getRightWall(movable, level.getGroundGrid());
        GameObject rightDynamicWall = SpatialGridHelper.getRightWall(movable, level.getDynamicGrid());

        if (rightWall != null) {
            if (rightDynamicWall != null) {

                float rightWallX = rightWall.getShape().getLeftX();
                float rightDynamicWallX = rightDynamicWall.getShape().getLeftX();
                if (rightWallX < rightDynamicWallX) {
                    return rightWall;
                } else {
                    return rightDynamicWall;
                }
            } else {
                return rightWall;
            }
        } else {
            return rightDynamicWall;
        }
    }
}
