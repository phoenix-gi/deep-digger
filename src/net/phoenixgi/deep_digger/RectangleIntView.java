package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.object.GameObject;
import net.phoenixgi.pgige.view.DebugView;

import java.awt.*;

public class RectangleIntView extends DebugView {
    public RectangleIntView(GameObject gameObject) {
        super(gameObject, 0x0000FF);
    }

    public RectangleIntView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        RectangleInt r = (RectangleInt) gameObject.getShape();
        g.setColor(new Color(color));
        g.drawRect((int) r.getCenter().x, (int) r.getCenter().y, (int) r.getWidth(), (int) r.getHeight());
    }
}

