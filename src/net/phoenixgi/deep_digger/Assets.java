package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Audio;
import net.phoenixgi.pgige.Sprite2D;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Assets {
    public static BufferedImage diggerMoving;
    public static Sprite2D diggerMovingSprite;
    public static BufferedImage diggerDigging;
    public static Sprite2D diggerDiggingSprite;
    public static BufferedImage diggerDiggingDown;
    public static Sprite2D diggerDiggingDownSprite;
    public static BufferedImage groundStoneTile;
    public static BufferedImage groundEarthTile;
    public static BufferedImage groundSandTile;
    public static BufferedImage groundTiles[];
    public static BufferedImage groundStone;
    public static BufferedImage groundEarth;
    public static BufferedImage groundSand;
    public static BufferedImage grounds[];

    public static BufferedImage rubyRed;
    public static BufferedImage rubyYellow;
    public static BufferedImage rubyPurple;
    public static BufferedImage rubies[];
    public static BufferedImage backgroundTile;
    public static BufferedImage background;

    public static Audio digSandSounds[] = new Audio[6];
    public static Audio digStoneSounds[] = new Audio[6];
    public static Audio digEarthSounds[] = new Audio[6];

    public static void load() {
        try {
            diggerMoving = ImageIO.read(Assets.class.getResource("/assets/digger-move.png"));
            diggerMovingSprite = new Sprite2D(diggerMoving, 48, 64);
            diggerDigging = ImageIO.read(Assets.class.getResource("/assets/digger-dig.png"));
            diggerDiggingSprite = new Sprite2D(diggerDigging, 64, 64);
            diggerDiggingDown = ImageIO.read(Assets.class.getResource("/assets/digger-dig-down.png"));
            diggerDiggingDownSprite = new Sprite2D(diggerDiggingDown, 64, 64);
            groundStoneTile = ImageIO.read(Assets.class.getResource("/assets/stone.png"));
            groundSandTile = ImageIO.read(Assets.class.getResource("/assets/sand.png"));
            groundEarthTile = ImageIO.read(Assets.class.getResource("/assets/earth.png"));
            {
                int cols = 3;
                int rows = 2;
                groundStone = new BufferedImage(groundStoneTile.getWidth() * cols, groundStoneTile.getHeight() * rows, BufferedImage.TYPE_INT_ARGB);
                groundEarth = new BufferedImage(groundEarthTile.getWidth() * cols, groundEarthTile.getHeight() * rows, BufferedImage.TYPE_INT_ARGB);
                groundSand = new BufferedImage(groundSandTile.getWidth() * cols, groundSandTile.getHeight() * rows, BufferedImage.TYPE_INT_ARGB);
                grounds = new BufferedImage[]{groundStone, groundSand, groundEarth};
                groundTiles = new BufferedImage[]{groundStoneTile, groundSandTile, groundEarthTile};
                for (int col = 0; col < cols; col++) {
                    for (int row = 0; row < rows; row++) {
                        for (int i = 0; i < grounds.length; i++) {
                            Graphics2D g = (Graphics2D) grounds[i].getGraphics();
                            BufferedImage tile = groundTiles[i];
                            g.drawImage(tile, col * tile.getWidth(), row * tile.getHeight(), null);
                        }
                    }
                }
            }
            rubyRed = ImageIO.read(Assets.class.getResource("/assets/ruby-red.png"));
            rubyYellow = ImageIO.read(Assets.class.getResource("/assets/ruby-yellow.png"));
            rubyPurple = ImageIO.read(Assets.class.getResource("/assets/ruby-purple.png"));
            rubies = new BufferedImage[]{rubyRed, rubyYellow, rubyPurple};
            backgroundTile = ImageIO.read(Assets.class.getResource("/assets/background.png"));
            background = new BufferedImage(1024, 576, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = (Graphics2D) background.getGraphics();
            for (int x = 0; x < 1024; x += backgroundTile.getWidth()) {
                for (int y = 0; y < 576; y += backgroundTile.getHeight()) {
                    g.drawImage(backgroundTile, x, y, null);
                }
            }
            for (int i = 0; i < 6; i++) {
                digStoneSounds[i] = new Audio("/assets/audio/dig-stone-" + (i + 1) + ".wav");
                digSandSounds[i] = new Audio("/assets/audio/dig-sand-" + (i + 1) + ".wav");
                digEarthSounds[i] = new Audio("/assets/audio/dig-earth-" + (i + 1) + ".wav");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
