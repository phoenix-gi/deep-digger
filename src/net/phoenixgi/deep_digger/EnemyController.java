package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Vector2;

import java.util.Random;
import java.util.Vector;

public class EnemyController extends DiggerController {
    Level level;

    public Vector pathToDigger;
    public int currentPath = 0;

    public EnemyController(Digger gameObject, MainScene scene) {
        super(gameObject, scene);
        level = scene.level;
        pathToDigger = new Vector();
    }

    private void calculatePathToDigger(Digger digger) {
        pathToDigger.clear();
        currentPath = 0;
        Digger enemy = (Digger) gameObject;
        int h = (int) (enemy.getShape().getCenter().y - digger.getShape().getCenter().y);
        int w = (int) Math.abs(enemy.getShape().getCenter().x - digger.getShape().getCenter().x);
        Random r = new Random();
        int numberOfPoints = 2 + r.nextInt(5);
        pathToDigger.add(enemy.getShape().getCenter().clone());
        for (int i = 1; i < numberOfPoints - 1; i++) {

        }
        pathToDigger.add(digger.getShape().getCenter().clone());
    }

    public void moveEnemyAccordingPath() {
        Digger enemy = (Digger) gameObject;
        Digger digger = level.getDigger();
        Vector2 p1 = (Vector2) pathToDigger.get(currentPath);
        Vector2 p2 = (Vector2) pathToDigger.get(currentPath + 1);
        Vector2 dist = p2.clone().sub(p1);
        if (dist.len() > 32) {
            if (dist.y < dist.x && dist.y > -dist.x) {
                moveRight();
                tryDigRight();
            } else if (dist.y > dist.x && dist.y < -dist.x) {
                moveLeft();
                tryDigLeft();
            } else if (dist.y > dist.x && dist.y > -dist.x) {
                stopMoveHorizontal();
                tryDigDown();
            } else {
                stopMoveHorizontal();
                stopDigging();
            }
        } else {
            stopMoveHorizontal();
            stopDigging();
            enemy.attack();
        }
    }

    @Override
    public void update(float deltaTime) {
        Digger enemy = (Digger) gameObject;
        Digger digger = level.getDigger();
        calculatePathToDigger(digger);
        moveEnemyAccordingPath();
        super.update(deltaTime);
    }
}
