package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.*;
import java.awt.event.KeyEvent;

public class DescriptionScene extends Scene {
    public DescriptionScene(SceneManager sceneManager, Game game) {
        super(sceneManager, game);
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_SPACE:
                        setNextScene();
                        break;
                    case KeyEvent.VK_F11:
                        getGame().setFullScreen(!getGame().isFullScreen());
                        break;
                }
            }
        });
    }

    public void setNextScene() {
        getSceneManager().setScene(new MainScene(getSceneManager(), getGame()));
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g = (Graphics2D) drawingContext;
        int width = getWidth();
        int height = getHeight();
        g.setColor(new Color(0xFFB960));
        g.fillRect(0, 0, width, height);
        g.setColor(Color.black);
        Font font = new Font("Arial", Font.BOLD, 32);
        Font small = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();
        String howToPlayText = "How to play";
        g.drawString(howToPlayText, width / 2 - metrics.stringWidth(howToPlayText) / 2, 64);
        g.setFont(small);
        g.setColor(new Color(155, 62, 6));
        String purpose = "Your purpose is always go down and make score as much as possible";
        g.drawString(purpose, 64, 64 + 48);
        g.setColor(Color.BLACK);
        g.drawString("Press LEFT KEY to move left or dig left", 64, 64 + 48 * 2);
        g.drawString("Press RIGHT KEY to move right or dig right", 64, 64 + 48 * 3);
        g.drawString("Press DOWN KEY to dig down", 64, 64 + 48 * 4);
        g.drawString("Press SPACE to attack enemies", 64, 64 + 48 * 5);
        g.drawString("You CAN'T jump - miner is too heavy, sorry:)", 64, 64 + 48 * 6);
        g.drawString("Press F11 to make full screen", 64, 64 + 48 * 7);
        g.drawString("Enjoy the game! phoenix-gi", 64, 64 + 48 * 8);
        g.drawString(purpose, 64, 64 + 48);
        g.drawString(purpose, 64, 64 + 48);
        String skipTitle = "press SPACE to start game";
        font = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        g.setColor(Color.BLACK);
        metrics = g.getFontMetrics();
        g.drawString(skipTitle, width / 2 - metrics.stringWidth(skipTitle) / 2, height - font.getSize() - 32);
    }

    @Override
    public void destroy() {

    }
}
