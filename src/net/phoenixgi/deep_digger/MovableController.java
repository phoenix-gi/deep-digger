package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;

public class MovableController extends ObjectController {

    private MovableInFallController inFallController;
    private MovableMovementController movementController;
    private MovableOnGroundController onGroundController;
    MainScene scene;

    public MovableController(Movable gameObject, MainScene scene) {
        super(gameObject);
        inFallController = new MovableInFallController(gameObject, scene);
        movementController = new MovableMovementController(gameObject, scene);
        onGroundController = new MovableOnGroundController(gameObject, scene);
        this.scene = scene;
    }

    @Override
    public void update(float deltaTime) {
        movementController.update(deltaTime);
        getCurrentController().update(deltaTime);
    }

    private ObjectController getCurrentController() {
        Movable movable = (Movable) gameObject;
        switch (movable.getState()) {
            case Movable.STATE_IN_FALL:
                return inFallController;
            case Movable.STATE_ON_GROUND:
                return onGroundController;
            default:
                return null;
        }
    }

    public void moveRight() {
        Movable movable = (Movable) gameObject;
        movable.setMovementState(Movable.STATE_MOVEMENT_RIGHT);
    }

    public void moveLeft() {
        Movable movable = (Movable) gameObject;
        movable.setMovementState(Movable.STATE_MOVEMENT_LEFT);
    }

    public void stopMoveHorizontal() {
        Movable movable = (Movable) gameObject;
        movable.setMovementState(Movable.STATE_MOVEMENT_NO);
    }
}
