package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Vector2;

public class Movable extends RectangleIntObject {
    public final static int STATE_ON_GROUND = 0;
    public final static int STATE_IN_FALL = 2;

    public final static int STATE_MOVEMENT_NO = 0;
    public final static int STATE_MOVEMENT_RIGHT = 1;
    public final static int STATE_MOVEMENT_LEFT = 2;

    public final static int FALL_SPEED = 400;

    private Vector2 velocity;

    private float movementSpeed = 25;

    private int state = STATE_IN_FALL;

    private int movementState = STATE_MOVEMENT_NO;

    public final static int DIRECTION_LEFT = 0;
    public final static int DIRECTION_RIGHT = 1;

    private int direction = DIRECTION_RIGHT;

    public Movable(int width, int height) {
        super(width, height);
        velocity = new Vector2();
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setMovementState(int movementState) {
        this.movementState = movementState;
    }

    public int getMovementState() {
        return movementState;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }

    public void setMovementSpeed(float movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public float getMovementSpeed() {
        return movementSpeed;
    }
}
