package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.GameUpdater;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.GameWindow;
import net.phoenixgi.pgige.pc.JPanelCanvas;

public class DeepDigger {
    public static void main(String[] args) {
        Assets.load();
        JPanelCanvas canvas = new JPanelCanvas(1024, 576);
        GameWindow window = new GameWindow(canvas, "Deep digger by phoenix-gi for ld-48");
        GameUpdater updater = new GameUpdater(window);
        SceneManager manager = new SceneManager();
        Scene scene = new TitleScene(manager, window);
        manager.setScene(scene);
        window.setSceneManager(manager);
        window.showWindow();
        updater.start();
    }
}
