package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.*;
import java.awt.event.KeyEvent;

public class TitleScene extends Scene {

    float timeLeft = 0;

    public TitleScene(SceneManager sceneManager, Game game) {
        super(sceneManager, game);
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                        setNextScene();
                        break;
                    case KeyEvent.VK_F11:
                        getGame().setFullScreen(!getGame().isFullScreen());
                        break;
                }
            }
        });
    }

    private void setNextScene() {
        getSceneManager().setScene(new DescriptionScene(getSceneManager(), getGame()));
    }

    @Override
    public void update(float deltaTime) {
        timeLeft += deltaTime;
        if (timeLeft >= 4) {
            setNextScene();
        }
    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g = (Graphics2D) drawingContext;
        int width = getWidth();
        int height = getHeight();
        g.setColor(new Color(0xFFB960));
        g.fillRect(0, 0, width, height);
        g.setColor(Color.black);
        Font font = new Font("Arial", Font.BOLD, 64);
        Font small = new Font("Arial", Font.ITALIC, 16);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();
        String author = "Deep digger";
        String special = "by phoenix-gi special for Ludum Dare 48";
        g.drawString(author, width / 2 - metrics.stringWidth(author) / 2, height / 2);
        g.setFont(small);
        metrics = g.getFontMetrics();
        g.setColor(new Color(155, 62, 6));
        g.drawString(special, width / 2 - metrics.stringWidth(special) / 2 + 64, height / 2 + 48);
        String skipTitle = "press enter or just wait to skip";
        font = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        g.setColor(Color.BLACK);
        metrics = g.getFontMetrics();
        g.drawString(skipTitle, width / 2 - metrics.stringWidth(skipTitle) / 2, height - font.getSize() - 32);
    }

    @Override
    public void destroy() {

    }
}
