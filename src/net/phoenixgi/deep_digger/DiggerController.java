package net.phoenixgi.deep_digger;

public class DiggerController extends MovableController {
    public DiggerController(Digger gameObject, MainScene scene) {
        super(gameObject, scene);
    }

    public void tryDigLeft() {
        Digger digger = (Digger) gameObject;
        digger.setDigDirection(Digger.DIG_LEFT);
        digger.setTryingDig(true);
    }

    public void tryDigRight() {
        Digger digger = (Digger) gameObject;
        digger.setDigDirection(Digger.DIG_RIGHT);
        digger.setTryingDig(true);
    }

    public void tryDigDown() {
        Digger digger = (Digger) gameObject;
        digger.setDigDirection(Digger.DIG_DOWN);
        digger.setTryingDig(true);
    }

    public void stopDigging() {
        Digger digger = (Digger) gameObject;
        digger.setDigDirection(Digger.DIG_NO_DIRECTION);
        digger.setTryingDig(false);
        digger.setDigging(false);
    }

    public void attack() {
        Digger digger = (Digger) gameObject;
        digger.attack();
    }
}
