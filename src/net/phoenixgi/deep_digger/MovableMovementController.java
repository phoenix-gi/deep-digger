package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;

/**
 * @author phoenix-gi
 */
public class MovableMovementController extends ObjectController {

    private MainScene scene;
    private MovableInMoveRightController inMoveRightController;
    private MovableInMoveLeftController inMoveLeftController;
    private MovableNoMoveController noMoveController;

    public MovableMovementController(Movable gameObject, MainScene scene) {
        super(gameObject);
        this.scene = scene;
        inMoveRightController = new MovableInMoveRightController(gameObject, scene);
        inMoveLeftController = new MovableInMoveLeftController(gameObject, scene);
        noMoveController = new MovableNoMoveController(gameObject);
    }

    public void update(float deltaTime) {
        getCurrentController().update(deltaTime);
    }

    private ObjectController getCurrentController() {
        Movable movable = (Movable) gameObject;
        switch (movable.getMovementState()) {
            case Movable.STATE_MOVEMENT_NO:
                return noMoveController;
            case Movable.STATE_MOVEMENT_LEFT:
                return inMoveLeftController;
            case Movable.STATE_MOVEMENT_RIGHT:
                return inMoveRightController;
            default:
                return null;
        }
    }

    public MovableInMoveRightController getInMoveRightController() {
        return inMoveRightController;
    }

    public MovableInMoveLeftController getInMoveLeftController() {
        return inMoveLeftController;
    }
}
