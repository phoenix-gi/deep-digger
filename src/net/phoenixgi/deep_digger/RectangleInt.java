package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.shape.Shape;

public class RectangleInt extends Shape {
    int width;
    int height;

    public RectangleInt(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void rotate(float angle) {

    }

    @Override
    public float getLeftX() {
        return getCenter().x;
    }

    @Override
    public float getRightX() {
        return getCenter().x + getWidth();
    }

    @Override
    public float getTopY() {
        return getCenter().y;
    }

    @Override
    public float getBottomY() {
        return getCenter().y + getHeight();
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }

    public void move(float x, float y) {
        getCenter().add(x, y);
    }
}
