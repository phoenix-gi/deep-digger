package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.object.GameObject;

public class MovableNoMoveController extends ObjectController {

    public MovableNoMoveController(GameObject gameObject) {
        super(gameObject);
    }

    public void update(float deltaTime) {
        Movable movable = (Movable) gameObject;
        movable.getVelocity().x = 0;
    }
}
