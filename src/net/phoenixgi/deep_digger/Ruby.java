package net.phoenixgi.deep_digger;

public class Ruby extends RectangleIntObject {
    public static int TYPE_RED = 0;
    public static int TYPE_YELLOW = 1;
    public static int TYPE_PURPLE = 2;
    private int type;

    public Ruby(int width, int height) {
        super(width, height);
        type = TYPE_RED;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
