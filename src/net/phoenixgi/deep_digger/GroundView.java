package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.object.GameObject;

import java.awt.*;

public class GroundView extends RectangleIntView {

    public GroundView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    public GroundView(GameObject gameObject) {
        super(gameObject);
    }

    @Override
    public void render(Graphics g) {
        Ground ground = (Ground) gameObject;
        if (Config.debugMode) {
            g.setColor(new Color(getColor()));
            RectangleInt shape = ground.getShape();
            int h = (int) (shape.getHeight() * (float) ground.getHealth() / (float) ground.getHealthMax());
            g.fillRect((int) shape.getLeftX(), (int) shape.getTopY(), (int) shape.getWidth(), h);
            super.render(g);
        } else {
            RectangleInt shape = ground.getShape();
            int w = (int) (shape.getWidth() * (float) ground.getHealth() / (float) ground.getHealthMax());
            int h = (int) (shape.getHeight() * (float) ground.getHealth() / (float) ground.getHealthMax());
            int x = 0;
            int y = 0;
            x = (int) shape.getLeftX();
            y = (int) shape.getTopY();
            java.awt.Shape oldClip = g.getClip();
            g.setClip(x, y, (int) shape.getWidth(), (int) shape.getHeight());
            g.drawImage(Assets.grounds[ground.getType()], x, y, null);
            g.setColor(new Color(0, 0, 0, 127));
            g.fillRect(x, y, (int) shape.getWidth(), (int) shape.getHeight());

            g.setClip(x + ((int) shape.getWidth() - w) / 2, y + ((int) shape.getHeight() - h) / 2, w, h);
            g.drawImage(Assets.grounds[ground.getType()], x, y, null);
            g.setClip(oldClip);
        }
    }
}
