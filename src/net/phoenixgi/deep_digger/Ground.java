package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Audio;

import java.util.Random;

public class Ground extends RectangleIntObject {
    public static final int TYPE_STONE = 0;
    public static final int TYPE_SAND = 1;
    public static final int TYPE_EARTH = 2;
    private int type;
    private int health = 100;
    private int healthMax = 100;
    public float solidityTypes[] = new float[]{2.0f, 0.5f, 1.0f};
    public float solidity;

    public Ground(int width, int height) {
        super(width, height);
        type = TYPE_STONE;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
        this.solidity = solidityTypes[type];
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealthMax() {
        return healthMax;
    }

    public void dig(float power) {
        health -= (int) (power / solidity);
        if (health <= 0) {
            health = 0;
        }
        playDigSound();
    }

    public void playDigSound() {
        Random r = new Random();

        Audio[] sounds = Assets.digStoneSounds;
        switch (type) {
            case TYPE_EARTH:
                sounds = Assets.digEarthSounds;
                break;
            case TYPE_STONE:
                sounds = Assets.digStoneSounds;
                break;
            case TYPE_SAND:
                sounds = Assets.digSandSounds;
                break;
        }
        int n = r.nextInt(6);
        if (!sounds[n].isRunning()) {
            sounds[n].stop();
            sounds[n].play();
        }
    }
}
