package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.object.GameObject;

/**
 * @author phoenix-gi
 */
public class MovableInFallController extends ObjectController {

    private MainScene scene;

    public MovableInFallController(Movable gameObject, MainScene scene) {
        super(gameObject);
        this.scene = scene;
    }

    public void update(float deltaTime) {
        Movable movable = (Movable) gameObject;

        GameObject bottomSurface = scene.getBottomSurfaceFor(movable);
        float groundY = Level.LEVEL_HEIGHT - 1;
        if (bottomSurface != null) {
            groundY = bottomSurface.getShape().getTopY();
        }
        movable.getShape().move(0, Movable.FALL_SPEED * deltaTime);
        if (movable.getShape().getBottomY() >= groundY) {
            movable.setState(Movable.STATE_ON_GROUND);
            movable.getShape().getCenter().y = groundY - movable.getShape().getHeight() - 1;
        }
    }
}
