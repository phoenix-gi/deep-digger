package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.object.GameObject;

public class RectangleIntObject extends GameObject {
    public RectangleIntObject(int width, int height) {
        RectangleInt rectangle = new RectangleInt(width, height);
        setShape(rectangle);
    }

    @Override
    public RectangleInt getShape() {
        return (RectangleInt) super.getShape();
    }
}
