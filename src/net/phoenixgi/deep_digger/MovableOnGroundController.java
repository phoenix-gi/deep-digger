package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.object.GameObject;

public class MovableOnGroundController extends ObjectController {
    MainScene scene;

    public MovableOnGroundController(GameObject gameObject, MainScene scene) {
        super(gameObject);
        this.scene = scene;
    }

    @Override
    public void update(float deltaTime) {
        Movable movable = (Movable) gameObject;
        float movableBottomY = movable.getShape().getBottomY();
        GameObject bottomSurface = scene.getBottomSurfaceFor(movable);
        if (bottomSurface == null || movableBottomY + 1 < bottomSurface.getShape().getTopY()) {
            movable.setState(Movable.STATE_IN_FALL);
        } else {
            movable.getShape().getCenter().y = bottomSurface.getShape().getTopY() - movable.getShape().getHeight() - 1;
        }
    }
}
