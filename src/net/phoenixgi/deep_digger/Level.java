package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.SpatialGrid;
import net.phoenixgi.pgige.SpatialGridHelper;
import net.phoenixgi.pgige.Vector2;
import net.phoenixgi.pgige.shape.CrossTester;
import net.phoenixgi.pgige.shape.Rectangle;
import net.phoenixgi.pgige.shape.Shape;

import java.util.Random;
import java.util.Vector;

public class Level {
    public final static int GRID_CELL_SIZE = 128;
    public final static int LEVEL_WIDTH = 1024;
    public final static int LEVEL_HEIGHT = 10240;

    private SpatialGrid groundGrid;
    private Vector grounds;
    private Vector groundViews;
    private Vector rubies;
    private Vector rubyViews;
    private SpatialGrid rubiesGrid;
    private Vector enemies;
    private Vector enemyViews;
    private Vector enemyControllers;

    Digger digger;
    DiggerView diggerView;

    Ground leftWall;
    Ground rightWall;
    Ground topWall;
    Ground bottomWall;

    private int score = 0;
    private int lastHeight = 0;
    private SpatialGrid dynamicGrid;

    private MainScene scene;

    public Level(MainScene scene) {
        this.scene = scene;
        groundGrid = new SpatialGrid(LEVEL_WIDTH, LEVEL_HEIGHT, GRID_CELL_SIZE);
        dynamicGrid = new SpatialGrid(LEVEL_WIDTH, LEVEL_HEIGHT, GRID_CELL_SIZE);
        rubiesGrid = new SpatialGrid(LEVEL_WIDTH, LEVEL_HEIGHT, GRID_CELL_SIZE);
        groundViews = new Vector();
        grounds = new Vector();
        rubyViews = new Vector();
        rubies = new Vector();
        enemies = new Vector();
        enemyViews = new Vector();
        enemyControllers = new Vector();

        digger = new Digger(31, 63);
        digger.setMovementSpeed(200);
        diggerView = new DiggerView(digger);
        digger.getShape().setCenter(new Vector2(0, 64));
    }

    private int y = 200;
    private int generated = 200;

    public void generateNext() {
        Random r = new Random();
        for (int row = 0; row < 8; row++) {
            int x = 0;
            int height = 63;
            while (x < LEVEL_WIDTH) {
                int width = 40 + r.nextInt(32);
                if (x + width > LEVEL_WIDTH) {
                    width = LEVEL_WIDTH - x;
                }
                Ground ground = new Ground(width, height);
                ground.getShape().getCenter().x = x;
                ground.getShape().getCenter().y = y;
                if (y + height > LEVEL_HEIGHT) {
                    return;
                }
                ground.setType(r.nextInt(3));
                if (r.nextInt(10) > 3) {
                    grounds.add(ground);
                    GroundView view = new GroundView(ground);
                    groundViews.add(view);
                    try {
                        groundGrid.insertObject(ground);
                    } catch (Exception e) {
                        grounds.remove(ground);
                        groundViews.remove(view);
                    }
                } else {
                    if (r.nextInt(10) > 3) {
                        Ruby ruby = new Ruby(width, height);
                        ruby.setType(r.nextInt(3));
                        ruby.getShape().getCenter().x = x;
                        ruby.getShape().getCenter().y = y;
                        rubies.add(ruby);
                        RubyView view = new RubyView(ruby);
                        rubyViews.add(view);
                        try {
                            rubiesGrid.insertObject(ruby);
                        } catch (Exception e) {
                            rubies.remove(ruby);
                            rubyViews.remove(view);
                        }
                    }
                }
                x += width + 1;
            }
            y += height + 1;
        }
        for (Object ruby : rubies) {
            Ruby rb = (Ruby) ruby;
            Ground gr = (Ground) SpatialGridHelper.getBottomSurfaceFor(rb, groundGrid);
            if (gr != null) {
                if ((gr.getShape().getTopY() - rb.getShape().getBottomY()) > 4) {
                    rubiesGrid.removeObject(rb);
                    rubyViews.remove(rb.getView());
                }
            }
        }
        generated = y;
    }

    private void checkHeightChanges() {
        int newHeight = ((int) getDigger().getShape().getCenter().y - 200) / 64;
        if (newHeight > lastHeight) {
            score += newHeight - lastHeight;
            lastHeight = newHeight;
            if (lastHeight % 10 == 0) {
                if (enemies.size() < 20) {
                    addNewEnemy();
                }
            }
        }
    }

    public void addNewEnemy() {
        Digger enemy = new Digger(31, 63);
        Random r = new Random();
        enemy.getShape().getCenter().x = r.nextInt(1024 - 32);
        enemy.getShape().getCenter().y = digger.getShape().getTopY() - 100;
        enemy.setMovementSpeed(25 + r.nextInt(25));
        enemy.setDigPower(50 + r.nextFloat() * 50);
        enemy.setAttackPower(1 + r.nextFloat() * 10);
        enemy.setAttackDelay(2f);
        enemy.getMoveAnimation().speed = 8f;
        enemies.add(enemy);
        DiggerView enemyView = new DiggerView(enemy);
        enemyView.setShowHealth(true);
        enemyViews.add(enemyView);
        EnemyController enemyController = new EnemyController(enemy, scene);
        enemy.setController(enemyController);
        enemyControllers.add(enemyController);

        Vector pColliders = groundGrid.getPotencialColliders(enemy);
        for (Object ground : pColliders) {
            Ground g = (Ground) ground;

            Shape dShape = enemy.getShape();
            Rectangle dRect = new Rectangle(dShape.getWidth(), dShape.getHeight());
            dRect.move((dShape.getCenter().x + dShape.getWidth() / 2), (dShape.getCenter().y + dShape.getHeight() / 2));
            Shape gShape = g.getShape();
            Rectangle gRect = new Rectangle(gShape.getWidth(), gShape.getHeight());
            gRect.move((gShape.getCenter().x + gShape.getWidth() / 2), (gShape.getCenter().y + gShape.getHeight() / 2));
            if (CrossTester.isCross(gRect, dRect)) {
                grounds.remove(g);
                groundViews.remove(g.getView());
                groundGrid.removeObject(g);
            }
        }
    }

    public SpatialGrid getGroundGrid() {
        return groundGrid;
    }

    public void setGroundGrid(SpatialGrid groundGrid) {
        this.groundGrid = groundGrid;
    }

    public Vector getGrounds() {
        return grounds;
    }

    public Vector getGroundViews() {
        return groundViews;
    }

    public void setGrounds(Vector grounds) {
        this.grounds = grounds;
    }

    public void setGroundViews(Vector groundViews) {
        this.groundViews = groundViews;
    }

    public Digger getDigger() {
        return digger;
    }

    public DiggerView getDiggerView() {
        return diggerView;
    }

    public SpatialGrid getDynamicGrid() {
        return dynamicGrid;
    }

    public void setDigger(Digger digger) {
        this.digger = digger;
    }

    public void setDiggerView(DiggerView diggerView) {
        this.diggerView = diggerView;
    }

    public void setDynamicGrid(SpatialGrid dynamicGrid) {
        this.dynamicGrid = dynamicGrid;
    }

    public void update(float deltaTime) {
        updateDigger(getDigger(), deltaTime);
        for (Object enemy : enemyControllers) {
            EnemyController controller = (EnemyController) enemy;
            Digger d = (Digger) controller.getGameObject();
            controller.update(deltaTime);
            updateDigger(d, deltaTime);
        }

        if (digger.isInAttack() && !digger.isAttackMade()) {
            Vector deadEnemies = new Vector();
            for (Object enemy : enemies) {
                Digger d = (Digger) enemy;
                if (d.isUnderAttack(digger)) {
                    d.takeDamage(digger);
                    if (d.getHealth() <= 0) {
                        deadEnemies.add(d);
                    }
                }
            }
            digger.makeAttack();
            for (Object e : deadEnemies) {
                Digger d = (Digger) e;
                enemies.remove(e);
                enemyViews.remove(d.getView());
                enemyControllers.remove(d.getController());
            }
        }
        for (Object enemy : enemies) {
            Digger d = (Digger) enemy;
            if (d.isInAttack() && !d.isAttackMade()) {
                if (digger.isUnderAttack(d)) {
                    digger.takeDamage(d);
                }
                d.makeAttack();
            }
        }
        Vector pColliders = rubiesGrid.getPotencialColliders(digger);
        for (Object ruby : pColliders) {
            Ruby r = (Ruby) ruby;

            Shape dShape = digger.getShape();
            Rectangle dRect = new Rectangle(dShape.getWidth(), dShape.getHeight());
            dRect.move((dShape.getCenter().x + dShape.getWidth() / 2), (dShape.getCenter().y + dShape.getHeight() / 2));
            Shape rShape = r.getShape();
            Rectangle rRect = new Rectangle(rShape.getWidth(), rShape.getHeight());
            rRect.move((rShape.getCenter().x + rShape.getWidth() / 2), (rShape.getCenter().y + rShape.getHeight() / 2));
            if (CrossTester.isCross(rRect, dRect)) {
                rubies.remove(r);
                rubyViews.remove(r.getView());
                rubiesGrid.removeObject(r);
                score += 1;
            }
        }
        checkHeightChanges();
        if (digger.getShape().getBottomY() > generated - 576) {
            if (digger.getShape().getBottomY() >= LEVEL_HEIGHT - 64) {
                digger.getShape().getCenter().y = 0;
                y = 200;
                generated = 200;
                groundViews.clear();
                rubyViews.clear();
                rubies.clear();
                grounds.clear();
                enemies.clear();
                enemyViews.clear();
                enemyControllers.clear();
                groundGrid.clear();
                rubiesGrid.clear();
            } else {
                clearUpperObjects();
            }
            generateNext();
        }
    }

    public void clearUpperObjects() {
        Vector forRemove = new Vector();
        for (Object ground : grounds) {
            Ground g = (Ground) ground;
            if (digger.getShape().getTopY() - g.getShape().getTopY() > 1024) {
                forRemove.add(g);
            }
        }
        for (Object ground : forRemove) {
            Ground g = (Ground) ground;
            grounds.remove(g);
            groundGrid.removeObject(g);
            groundViews.remove(g.getView());
        }
        forRemove.clear();
        for (Object ruby : rubies) {
            Ruby r = (Ruby) ruby;
            if (digger.getShape().getTopY() - r.getShape().getTopY() > 1024) {
                forRemove.add(r);
            }
        }
        for (Object ruby : forRemove) {
            Ruby r = (Ruby) ruby;
            rubies.remove(r);
            rubiesGrid.removeObject(r);
            rubyViews.remove(r.getView());
        }
        forRemove.clear();
    }

    private void updateDigger(Digger digger, float deltaTime) {
        leftWall = (Ground) SpatialGridHelper.getLeftWall(digger, getGroundGrid());
        rightWall = (Ground) SpatialGridHelper.getRightWall(digger, getGroundGrid());
        topWall = (Ground) SpatialGridHelper.getTopSurfaceYFor(digger, getGroundGrid());
        bottomWall = (Ground) SpatialGridHelper.getBottomSurfaceFor(digger, getGroundGrid());

        if (digger.isTryingDig()) {
            Ground ground = null;
            boolean canDig = false;
            switch (digger.getDigDirection()) {
                case Digger.DIG_LEFT:
                    ground = (Ground) SpatialGridHelper.getLeftWall(digger, groundGrid);
                    canDig = ground != null && Math.abs(ground.getShape().getRightX() - digger.getShape().getLeftX()) < 2;
                    break;
                case Digger.DIG_RIGHT:
                    ground = (Ground) SpatialGridHelper.getRightWall(digger, groundGrid);
                    canDig = ground != null && Math.abs(ground.getShape().getLeftX() - digger.getShape().getRightX()) < 2;
                    break;
                case Digger.DIG_UP:
                    break;
                case Digger.DIG_DOWN:
                    ground = (Ground) SpatialGridHelper.getBottomSurfaceFor(digger, groundGrid);
                    canDig = ground != null && ground.getShape().getTopY() - digger.getShape().getBottomY() < 2;
                    break;
            }
            digger.setDigging(canDig);
            if (canDig) {
                digger.updateDigging(deltaTime);
                if (digger.readyForDig()) {
                    ground.dig(digger.getDigPower());
                    if (ground.getHealth() == 0) {
                        groundGrid.removeObject(ground);
                        groundViews.remove(ground.getView());
                        grounds.remove(ground);
                    }
                    digger.resetDigging();
                }
            } else {
                digger.resetDigging();
            }
        }
        digger.updateAnimation(deltaTime);
        digger.updateAttack(deltaTime);
    }

    public Ground getLeftWall() {
        return leftWall;
    }

    public Ground getRightWall() {
        return rightWall;
    }

    public Ground getTopWall() {
        return topWall;
    }

    public Ground getBottomWall() {
        return bottomWall;
    }

    public Vector getRubyViews() {
        return rubyViews;
    }

    public Vector getRubies() {
        return rubies;
    }

    public Vector getEnemyViews() {
        return enemyViews;
    }

    public int getScore() {
        return score;
    }
}
