package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.Game;
import net.phoenixgi.pgige.Scene;
import net.phoenixgi.pgige.SceneManager;
import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.*;
import java.awt.event.KeyEvent;

public class GameOverScene extends Scene {
    int score;

    public GameOverScene(SceneManager sceneManager, Game game, int score) {
        super(sceneManager, game);
        this.score = score;
    }

    @Override
    public void initialize() {
        setInput(new StandardInput() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                        setNextScene();
                        break;
                    case KeyEvent.VK_F11:
                        getGame().setFullScreen(!getGame().isFullScreen());
                        break;
                }
            }
        });
    }

    private void setNextScene() {
        getSceneManager().setScene(new TitleScene(getSceneManager(), getGame()));
    }

    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void render(Object drawingContext) {
        Graphics2D g = (Graphics2D) drawingContext;
        int width = getWidth();
        int height = getHeight();
        g.setColor(new Color(0xFFB960));
        g.fillRect(0, 0, width, height);
        g.setColor(Color.black);
        Font font = new Font("Arial", Font.BOLD, 64);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics();
        String gameOverText = "Game over";
        g.drawString(gameOverText, width / 2 - metrics.stringWidth(gameOverText) / 2, height / 2);
        font = new Font("Arial", Font.BOLD, 24);
        g.setFont(font);
        metrics = g.getFontMetrics();
        String result = "your score: " + score;
        g.drawString(result, width / 2 - metrics.stringWidth(result) / 2, height / 2 + 80);
        String resetText = "press enter to reset game";
        font = new Font("Arial", Font.BOLD, 16);
        g.setFont(font);
        g.setColor(Color.BLACK);
        metrics = g.getFontMetrics();
        g.drawString(resetText, width / 2 - metrics.stringWidth(resetText) / 2, height - font.getSize() - 32);
    }

    @Override
    public void destroy() {

    }
}
