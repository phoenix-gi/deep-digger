package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.pc.StandardInput;

import java.awt.event.KeyEvent;

public class DiggerInput extends StandardInput {
    DiggerController controller;
    Level level;

    public DiggerInput(DiggerController controller, Level level) {
        this.controller = controller;
        this.level = level;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                controller.moveLeft();
                controller.tryDigLeft();
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                controller.moveRight();
                controller.tryDigRight();
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                controller.tryDigDown();
                break;
            case KeyEvent.VK_SPACE:
                controller.attack();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                controller.stopMoveHorizontal();
                controller.stopDigging();
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                controller.stopDigging();
                break;
        }
    }
}
