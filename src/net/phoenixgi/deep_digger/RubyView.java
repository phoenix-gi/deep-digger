package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.object.GameObject;

import java.awt.*;

public class RubyView extends RectangleIntView {
    public RubyView(GameObject gameObject) {
        super(gameObject);
    }

    public RubyView(GameObject gameObject, int color) {
        super(gameObject, color);
    }

    @Override
    public void render(Graphics g) {
        Ruby ruby = (Ruby) gameObject;
        int x = (int) ruby.getShape().getCenter().x;
        int y = (int) ruby.getShape().getCenter().y;
        int width = (int) ruby.getShape().getWidth();
        int height = (int) ruby.getShape().getHeight();
        int type = ruby.getType();
        g.drawImage(Assets.rubies[type], x + width / 2 - 16, y + height - 32, null);
    }
}
