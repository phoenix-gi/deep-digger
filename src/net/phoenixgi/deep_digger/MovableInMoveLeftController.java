package net.phoenixgi.deep_digger;

import net.phoenixgi.pgige.ObjectController;
import net.phoenixgi.pgige.object.GameObject;

/**
 * @author phoenix-gi
 */
public class MovableInMoveLeftController extends ObjectController {

    private MainScene scene;

    public MovableInMoveLeftController(Movable gameObject, MainScene scene) {
        super(gameObject);
        this.scene = scene;
    }

    public void update(float deltaTime) {
        Movable movable = (Movable) gameObject;

        movable.getVelocity().x = -movable.getMovementSpeed();

        GameObject leftWall = scene.getLeftWallFor(movable);
        float leftWallX = 0;
        if (leftWall != null) {
            leftWallX = leftWall.getShape().getRightX();
        }

        movable.getShape().move(movable.getVelocity().x * deltaTime, 0);
        if (movable.getShape().getLeftX() < leftWallX) {
            movable.getShape().getCenter().x = leftWallX + 1;
        }
        movable.setDirection(Movable.DIRECTION_LEFT);
    }
}
